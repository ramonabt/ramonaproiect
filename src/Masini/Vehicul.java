package Masini;

import java.math.BigDecimal;
import java.util.ArrayList;

public abstract class Vehicul {

    private String marca;
    private String culoare;
    private BigDecimal pret;
    private Integer an;

    //Agregare/Compozitie + Colectie
    protected ArrayList<Roata> roti;

    //Constructor
    public Vehicul()
    {
        this.roti = new ArrayList<>();
    }

    //Incapsulare
    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getCuloare() {
        return culoare;
    }

    public void setCuloare(String culoare) {
        this.culoare = culoare;
    }

    public BigDecimal getPret() {
        return pret;
    }

    public void setPret(BigDecimal pret) {
        this.pret = pret;
    }

    public Integer getAn() {
        return an;
    }

    public void setAn(Integer an) {
        this.an = an;
    }

    public abstract int getNumarRoti();

    public abstract void seteazaRotile();
}
