package Masini;

public class Automobil extends Vehicul{
    private static final int NUMAR_ROTI = 4;
    public Automobil() {
    }

    @Override
    public int getNumarRoti()
    {
        return NUMAR_ROTI;
    }

    @Override
    public void seteazaRotile() {
        for(int i = 0; i < NUMAR_ROTI; i++){
            roti.add(new Roata());
        }
    }
}
