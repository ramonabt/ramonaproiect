package Masini;

public class VehiculFactory {

    public Vehicul getVehicul(String tipVehicul){
        if(tipVehicul == null)
        {
            return null;
        }
        if(tipVehicul.equalsIgnoreCase("TIR"))
        {
            return new Tir();
        }
        else if(tipVehicul.equalsIgnoreCase("AUTOMOBIL"))
        {
            return new Automobil();
        }
        else if(tipVehicul.equalsIgnoreCase("AUTOBUZ"))
        {
            return new Autobuz();
        }

        return null;
    }
}
