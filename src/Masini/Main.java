package Masini;

import java.math.BigDecimal;

public class Main {
    public static void main(String[] args) {

        //Thread
        (new Thread(new ReclamaThread())).start();

        //Factory design pattern
        VehiculFactory vehiculFactory = new VehiculFactory();

        //Creem un tir
        Vehicul tir = vehiculFactory.getVehicul("tir");
        tir.setCuloare("rosu");
        tir.setAn(1995);
        tir.setMarca("Scania");
        tir.setPret(new BigDecimal(1999.00));
        tir.seteazaRotile();

        //Creem un automobil
        Vehicul automobil = vehiculFactory.getVehicul("automobil");
        automobil.setCuloare("albastru");
        automobil.setAn(1995);
        automobil.setMarca("BMW");
        automobil.setPret(new BigDecimal(1999.00));
        automobil.seteazaRotile();

        //Creem un autobuz
        Vehicul autobuz = vehiculFactory.getVehicul("autobuz");
        autobuz.setCuloare("verde");
        autobuz.setAn(1995);
        autobuz.setMarca("Mercedes");
        autobuz.setPret(new BigDecimal(1999.00));
        autobuz.seteazaRotile();

        System.out.println("TIR");
        System.out.println("Numar roti " + tir.getNumarRoti());
        System.out.println("Marca " + tir.getMarca());
        System.out.println("Culoare " + tir.getCuloare());
        System.out.println("An " + tir.getAn());
        System.out.println("Pret " + tir.getPret());

        System.out.println("AUTOBUZ");
        System.out.println("Numar roti " + autobuz.getNumarRoti());
        System.out.println("Marca " + autobuz.getMarca());
        System.out.println("Culoare " + autobuz.getCuloare());
        System.out.println("An " + autobuz.getAn());
        System.out.println("Pret " + autobuz.getPret());

        System.out.println("AUTOMOBIL");
        System.out.println("Numar roti " + automobil.getNumarRoti());
        System.out.println("Marca " + automobil.getMarca());
        System.out.println("Culoare " + automobil.getCuloare());
        System.out.println("An " + automobil.getAn());
        System.out.println("Pret " + automobil.getPret());

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("Asteptam sa vedecm ce cumparam");
    }
}
