package Masini;

public class Tir extends Vehicul {
    private static final int NUMAR_ROTI = 18;
    public Tir() {
    }

    public int getNumarRoti()
    {
        return NUMAR_ROTI;
    }

    @Override
    public void seteazaRotile() {
        for(int i = 0; i < NUMAR_ROTI; i++){
            roti.add(new Roata());
        }
    }
}